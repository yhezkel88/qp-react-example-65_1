//
//  PRLogger.h
//  PrivacyRatingFramework
//
//  Created by Yossi Rating on 08/07/2019.
//  Copyright © 2019 PrivacyRating. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN



//#ifdef DEBUG

#define PRLogger_All( s, ... ) [[PRLogger shared] print: [self class] level: LoggerLevelAll func: [NSString stringWithFormat: @"%s", __PRETTY_FUNCTION__] line: __LINE__ str: [NSString stringWithFormat:(s), ##__VA_ARGS__]]

#define PRLogger_Log( s, ... ) [[PRLogger shared] print: [self class] level: LoggerLevelLog func: [NSString stringWithFormat: @"%s", __PRETTY_FUNCTION__] line: __LINE__ str: [NSString stringWithFormat:(s), ##__VA_ARGS__]]
#define PRLogger_Info( s, ... ) [[PRLogger shared] print: [self class] level: LoggerLevelInfo func: [NSString stringWithFormat: @"%s", __PRETTY_FUNCTION__] line: __LINE__ str: [NSString stringWithFormat:(s), ##__VA_ARGS__]]
#define PRLogger_Warn( s, ... ) [[PRLogger shared] print: [self class] level: LoggerLevelWarn func: [NSString stringWithFormat: @"%s", __PRETTY_FUNCTION__] line: __LINE__ str: [NSString stringWithFormat:(s), ##__VA_ARGS__]]
#define PRLogger_Error( s, ... ) [[PRLogger shared] print: [self class] level: LoggerLevelError func: [NSString stringWithFormat: @"%s", __PRETTY_FUNCTION__] line: __LINE__ str: [NSString stringWithFormat:(s), ##__VA_ARGS__]]
#define PRLogger_Fatal( s, ... ) [[PRLogger shared] print: [self class] level: LoggerLevelFatal func: [NSString stringWithFormat: @"%s", __PRETTY_FUNCTION__] line: __LINE__ str: [NSString stringWithFormat:(s), ##__VA_ARGS__]]
//#define PRLogger_Swizzled( s, ... ) [[PRLogger shared] print: [self class] level: LoggerLevelSwizzled func: [NSString stringWithFormat: @"%s", __PRETTY_FUNCTION__] line: __LINE__ str: [NSString stringWithFormat:(s), ##__VA_ARGS__]]
//
//
#define ClassKillSwitchLog  PRLogger_Info(@"Kill Switched %s", __PRETTY_FUNCTION__);
#define ClassFunctionLog  PRLogger_Swizzled(@"%s", __PRETTY_FUNCTION__);
#define LogUrl(fmt, ...) PRLogger_Swizzled((@"%s Url == " fmt), __PRETTY_FUNCTION__, ##__VA_ARGS__);
#define LogUrlRequest(fmt, ...) PRLogger_Swizzled((@"%s Url == " fmt), __PRETTY_FUNCTION__, ##__VA_ARGS__);
//
//#define PRLogger_SetClassLevel( l ) [[PRLogger shared] setLevel: [self class] level: (l)]

//#else
//#define PRLogger_All( s, ... )
//#define PRLogger_Log( s, ... )
//#define PRLogger_Info( s, ... )
//#define PRLogger_Warn( s, ... )
//#define PRLogger_Error( s, ... )
//#define PRLogger_Fatal( s, ... )
#define PRLogger_Swizzled( s, ... )
//
//#define PRLogger_SetClassLevel( level )
//
//#endif


typedef NS_ENUM(NSInteger, LoggerLevel) {
    LoggerLevelNone = 0,
    LoggerLevelLog = 1,
    LoggerLevelInfo = 2,
    LoggerLevelWarn = 4,
    LoggerLevelError = 8,
    LoggerLevelFatal = 16,
    LoggerLevelSwizzled = 32,
    LoggerLevelAll = (LoggerLevelLog | LoggerLevelInfo | LoggerLevelWarn | LoggerLevelError | LoggerLevelFatal),
};


@interface PRLogger : NSObject

- (void) print: (Class) theClass
         level:(LoggerLevel)level
          func:(NSString *) func
          line:(NSInteger)line
           str:(NSString *) str;

- (void) setLevel:(LoggerLevel) level;

+ (PRLogger *)shared;

@property (nonatomic) NSInteger logLevel;

@end

NS_ASSUME_NONNULL_END
