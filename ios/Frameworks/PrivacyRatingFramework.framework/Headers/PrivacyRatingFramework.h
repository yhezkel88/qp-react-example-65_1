//
//  PrivacyRatingFramework.h
//  PrivacyRatingFramework
//
//  Created by Ari Deane on 18/11/2018.
//  Copyright © 2018 PrivacyRating. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrivacyRatingManager.h"
#import "TestManager.h"
#import "CacheManager.h"
#import "QPData.h"
#import "Enums.h"
#import "PRLogger.h"

//! Project version number for PrivacyRatingFramework.
FOUNDATION_EXPORT double PrivacyRatingFrameworkVersionNumber;

//! Project version string for PrivacyRatingFramework.
FOUNDATION_EXPORT const unsigned char PrivacyRatingFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PrivacyRatingFramework/PublicHeader.h>





