//
//  NSDate+Monitoring.h
//  PrivacyRatingFramework
//
//  Created by Yossi Rating on 26/02/2019.
//  Copyright © 2019 PrivacyRating. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (Monitoring)

@end

NS_ASSUME_NONNULL_END
